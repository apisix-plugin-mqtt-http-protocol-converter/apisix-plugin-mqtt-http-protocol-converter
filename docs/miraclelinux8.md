# MIRACLELINUX 8向け環境構築方法

MIRACLELINUX 8向け環境構築方法について説明します。

## 前提条件

次の環境での動作を想定しています。

* MIRACLELINUX 8.4
* Apache APISIX 3.9.x, 3.10.0
* Etcd 3.5.4
* VerneMQ 1.12.4

本手順では、上記に加えてAPISIXプラグインのパッケージを導入することで、MQTTをHTTPへとするための環境を構築します。
なお、本手順ではすべて同一ホストでソフトウェアを動作させるものとします。

## インストール手順

インストールするには次の手順を実施します。

* Etcdのインストール
* VerneMQのインストール
* Apache APISIXのインストール
* LuaRocksのインストール
* APISIXプラグインのインストール

### Etcdのインストール

https://github.com/etcd-io/etcd/releases/tag/v3.5.4 のLinuxの導入手順を実行します。
/tmp/etcd-download-test以下にバイナリが配置されるので、次のコマンドで/usr/local/bin配下に移動し、サービスとして起動できるようにします。

```
$ curl -L -o /tmp/etcd-v3.5.4-linux-amd64.tar.gz https://storage.googleapis.com/etcd/v3.5.4/etcd-v3.5.4-linux-amd64.tar.gz
$ mkdir -p /tmp/etcd-download-test
$ tar zxvf /tmp/etcd-v3.5.4-linux-amd64.tar.gz -C /tmp/etcd-download-test --strip-components=1
$ sudo mv /tmp/etcd-download-test/etcd* /usr/local/bin
$ curl -L -O https://gitlab.com/apisix-plugin-mqtt-http-protocol-converter/apisix-plugin-mqtt-http-protocol-converter/-/raw/main/package/etcd.service
$ sudo mv etcd.service /usr/lib/systemd/system
$ sudo chown root:root /usr/lib/systemd/system/etcd.service
$ sudo chmod 644 /usr/lib/systemd/system/etcd.service
```

インストールできたら、serviceファイルが読み込まれるように、SELinuxの設定をします。
その際、一時的にSELinuxを無効化します。

```
$ sudo setenforce 0
$ sudo ausearch -c 'systemd' --raw | audit2allow -M enable-etcd
$ sudo semodule -X 300 -i enable-etcd.pp
$ sudo restorecon -Rv /usr/lib/systemd/system/etcd.service
```

これで、sudo systemctl list-unit-filesでサービスファイルの状態をみたときに、etcdがdisabledな状態になります。

次に、etcdを起動し、SELinuxのコンテキストの情報を収集します。

```
$ sudo systemctl daemon-reload
$ sudo systemctl enable etcd
$ sudo systemctl start etcd
```

SELinuxのための追加設定を次のコマンドで行います。

```
$ sudo ausearch -c '(etcd)' --raw | audit2allow -M etcd
$ sudo semodule -X 300 -i etcd.pp
$ sudo restorecon -Rv /usr/local/bin/etcd
$ sudo systemctl restart etcd
```

期待通りに設定できていると、etcdがサービスとして正常起動します。

最後に、一時的に変更したSELinuxの設定を戻しておきます。

```
$ sudo setenforce 1
```

### VerneMQのインストール

次のコマンドを実行して、VerneMQをインストールします。

```
$ curl -L -o vernemq-1.12.4.centos8.x86_64.rpm https://github.com/vernemq/vernemq/releases/download/1.12.4/vernemq-1.12.4.centos8.x86_64.rpm
$ sudo dnf install -y ./vernemq-1.12.4.centos8.x86_64.rpm
```

次に、EULAに同意したことを示すため、 `/etc/vernemq/vernemq.conf` の`accept_eula`の設定を`yes`へ変更します。

```
accept_eula = yes
```

次に、`/etc/vernemq/vernemq.conf`の`allow_anonymous`の設定を`on`へ変更します。

```
allow_anonymous = on
```

なお、上記は動作確認の簡略化のため認証を不要としただけであり、プロダクション環境では適宜設定を推奨します。


最後にサービスを起動します。

```
$ sudo systemctl enable vernemq
$ sudo systemctl start vernemq
```

動作確認のため、次のコマンドを実行してmosquittoをインストールします。


```
$ sudo dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
$ sudo dnf install -y mosquitto
```

次のコマンドを実行し、mosquitto_pubで認証不要でメッセージを送信できたら、期待通りにインストールできています。

```
$ mosquitto_pub -d -h localhost -t test -m "test_message"
```

### Apache APISIXのインストール

次のコマンドを実行して、Apache APISIXをインストールします。

```
$ sudo dnf install -y https://repos.apiseven.com/packages/centos/apache-apisix-repo-1.0-1.noarch.rpm
$ sudo dnf install -y apisix
```

本プラグインは、APISIXのバージョン3.8.0以降をサポートしています。
旧バージョンのAPISIXを使用する場合は、バージョンを指定してインストールしてください。
旧バージョンとの対応については、[CHANGELOG.md](../CHANGELOG.md)を参照してください。

次のコマンドは、3.8.0を使用する場合の例です。

```
$ sudo dnf install -y https://repos.apiseven.com/packages/centos/apache-apisix-repo-1.0-1.noarch.rpm
$ sudo dnf install -y apisix-3.8.0
```

APISIXは次の2つのパッケージから構成されています。

* apisix (APISIXおよびLuaのモジュールを含んだパッケージ)
* apisix-base (apisix向けのOpenRestyを含んだパッケージ。公式手順ではCentOS 7しか案内されていないが、CentOS 8向けのパッケージが使える。OpenResty公式が提供するopenrestyパッケージをインストールしているとコンフリクトするため使わない。)

インストールできたら、サービスを起動します。

```
$ sudo systemctl enable apisix
$ sudo systemctl start apisix
```

## LuaRocksのインストール

Luaモジュールを管理するために、次のコマンドでLuaRocksをインストールします。

```
$ curl -L -o linux-install-luarocks.sh https://raw.githubusercontent.com/apache/apisix/master/utils/linux-install-luarocks.sh
$ chmod +x linux-install-luarocks.sh
$ sudo dnf install -y wget make
$ sudo ./linux-install-luarocks.sh
```

次のコマンドを実行してバージョンが表示できたら、期待通りにインストールできています。

```
$ luarocks --version
/usr/local/bin/luarocks 3.8.0
LuaRocks main command-line interface
```

### LuaRocksの設定

LuaRocksから必要なモジュールを認識できるようにするために、設定ファイルを変更します。

対象となるファイルは `/usr/local/etc/luarocks/config-5.1.lua` です。
rocks_treesにapisixとプラグイン(apisix-plugin-mqtt-http-protocol-converter)のためのパスを追加します。

```
rocks_trees = {
   { name = "user", root = home .. "/.luarocks" };
   { name = "system", root = "/usr/local/" };
   { name = "apisix", root = "/usr/local/apisix/deps" };
   { name = "apisix-plugin-mqtt-http-protocol-converter", root = "/usr/local/apisix-plugin-mqtt-http-protocol-converter/deps" };
}
```

### APISIXプラグインのインストール

プラグインをインストールするには以下の手順を実施します。

#### パッケージファイルのダウンロード

https://gitlab.com/apisix-plugin-mqtt-http-protocol-converter/apisix-plugin-mqtt-http-protocol-converter/-/releases/1.2.6 から以下のパッケージファイルをダウンロードします。

* apisix-plugin-mqtt-http-protocol-converter-1.2.6-1.el8.x86_64.rpm
* apisix-plugin-mqtt-http-protocol-converter-config-apisix-3-1.2.6-1.el8.x86_64.rpm

#### パッケージのインストール

パッケージファイルを保存したディレクトリに移動し、パッケージをインストールします。

```
$ sudo dnf install -y ./apisix-plugin-mqtt-http-protocol-converter-config-apisix-3-1.2.6-1.el8.x86_64.rpm ./apisix-plugin-mqtt-http-protocol-converter-1.2.6-1.el8.x86_64.rpm
```

#### APISIXサービスの再起動

プラグインのインストールができたら、APISIXサービスを再起動します。

```
$ sudo systemctl restart apisix
```

## 動作確認手順

[README.md](../README.md#usage)のUsageに記載の手順でAPISIXプラグインの動作確認を行えます。

上記Usageでは次の挙動を想定しています。

* mqtt-httpプラグインは9100ポートと9101ポートでMQTTメッセージを待ち受ける
* mqtt-httpプラグインはHTTPに変換したメッセージを 8001もしくは8002ポートへラウンドロビン方式で送信する
* サブスクライバーの管理用にsubscribe,unsubscribe,statusの3つの公開APIをrouteとして登録する
* subscribe時にはVerneMQのURIやMQTTメッセージの転送先のURI、サブスクライブするトピックを登録する
* 変換されたHTTPメッセージを受信するHTTPサーバーを8001、8002ポートで起動する
* mosquitto_pubでMQTTメッセージを送信することで、HTTPサーバーにメッセージが到達することを確認する


具体的には次の手順を実行します。

* `mqtt-http`プラグインを登録する
* 公開APIを有効にする
* MQTTブローカー(VerneMQ)に登録する
* HTTPサーバーを起動する
* MQTTメッセージをMQTTブローカーに送信する

### `mqtt-http`プラグインを登録する

`mqtt-http`プラグインを登録するには次のコマンドを実行します。

```
$ curl http://127.0.0.1:9180/apisix/admin/stream_routes/1 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "server_addr": "127.0.0.1",
    "server_port": 9100,
    "plugins": {
        "mqtt-http": {
            "protocol_version": 4,
            "path": "/path/to/test"
        }
    },
    "upstream": {
        "type": "roundrobin",
        "nodes": [{
            "host": "127.0.0.1",
            "port": 8001,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8002,
            "weight": 1
        }
        ]
    }
}'
$ curl http://127.0.0.1:9180/apisix/admin/stream_routes/2 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "server_addr": "127.0.0.1",
    "server_port": 9101,
    "plugins": {
        "mqtt-http": {
            "protocol_version": 5,
            "path": "/path/to/test"
        }
    },
    "upstream": {
        "type": "roundrobin",
        "nodes": [{
            "host": "127.0.0.1",
            "port": 8001,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8002,
            "weight": 1
        }
        ]
    }
}'
```

上記のコマンドで、9100,9101ポートでMQTTのメッセージを受け付け、HTTPに変換したものを`http://127.0.0.1:8001/path/to/test`もしくは`http://127.0.0.1:8002/path/to/test`へ送信します。

### 公開APIを有効にする

公開APIを登録するには、次のコマンドを実行します。

```
$ curl 'http://127.0.0.1:9180/apisix/admin/routes/1' -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/apisix/plugin/mqtt-http-subscriber/unsubscribe",
    "plugins": {
        "public-api": {}
    }
}'
$ curl 'http://127.0.0.1:9180/apisix/admin/routes/2' -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/apisix/plugin/mqtt-http-subscriber/subscribe",
    "plugins": {
        "public-api": {}
    }
}'
$ curl 'http://127.0.0.1:9180/apisix/admin/routes/3' -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/apisix/plugin/mqtt-http-subscriber/status",
    "plugins": {
        "public-api": {}
    }
}'
```

### MQTTブローカーに登録する

MQTTブローカーに登録するには、次のコマンドを実行します。

```
$ curl -X POST -H "Content-Type: application/json" "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/subscribe" -d '
{
    "broker_uri" : "127.0.0.1:1883" ,
    "upstream_uri" : "127.0.0.1:9101",
    "topic" : "example/#"
}'
```

コマンドの実行に成功すると、`Subscribed`と表示されます。

以下のパラメータが指定可能です。

| パラメータ名         | 型             | 省略時のデフォルト値                                             | 説明                                                             |
|----------------------|----------------|------------------------------------------------------------------|------------------------------------------------------------------|
| broker_uri           | 文字列         | "localhost:1883"                                                 | ブローカーのURI。                                                |
| upstream_uri         | 文字列         | "localhost:9100"                                                 | 転送先のmqtt-httpプラグインのURI。                               |
| topic                | 文字列         | "/#"                                                             | サブスクライブするトピック。                                     |
| mqtt_version         | 文字列         | "5"                                                              | MQTTのバージョン。"4" （MQTT v3.1.1）または"5"（MQTT v5.0）。    |
| user_name            | 文字列         | stPwSVV73Eqw5LSv0iMXbc4EguS7JyuZR9lxU5uLxI5tiNM8ToTVqNpu85pFtJv9 | ブローカーに接続する際のユーザー名。                             |
| password             | 文字列         | なし                                                             | ブローカーに接続する際のパスワード。                             |
| keep_alive           | 整数           | 60                                                               | サブスクライバーとブローカー間でのkeep_aliveの間隔（秒数）。     |
| connect_properties   | JSON形式文字列 | なし                                                             | ブローカーに`CONNECT`を送信する時のプロパティ。 ※1               |
| subscribe_properties | JSON形式文字列 | なし                                                             | ブローカーに`SUBSCRIBE`を送信する時のプロパティ。 ※1             |
| reconnect            | 真偽値         | false                                                            | ブローカーとの通信でエラーになった際、自動で再接続するかどうか。 |

※1

プロパティは [json_key_and_mqtt_spec.md](json_key_and_mqtt_spec.md) のJSON key nameの形式で指定する必要があります。

使用可能なプロパティは以下のリンクを参照してください。

[CONNECT Properties](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901046)
[SUBSCRIBE Properties](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901164)

また、現在以下のプロパティはサポート対象外です。

* authentication_method
  * 現在本プラグインがAUTHパケットをサポートしていないため
* authentication_data
  * 現在本プラグインがAUTHパケットをサポートしていないため
* receive_maximum
  * 現在本プラグインでは常に1なため


例として、ユーザー名、パスワードを指定する場合は次のコマンドを実行します。

```
$ curl -X POST -H "Content-Type: application/json" "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/subscribe" -d '
{
"broker_uri" : "127.0.0.1:1883" ,
"upstream_uri" : "127.0.0.1:9101",
"user_name" : "user",
"password" : "password",
"topic" : "example/#"
}'
```

`connect_properties`、`subscribe_properties` を指定する場合は次のコマンドを実行します。

```
$ curl -X POST -H "Content-Type: application/json" "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/subscribe" -d '
{
    "broker_uri" : "127.0.0.1:1883" ,
    "upstream_uri" : "127.0.0.1:9101",
    "topic" : "example/#",
    "connect_properties": {
        "session_expiry_interval": 10,
        "maximum_packet_size": 1024
    },
    "subscribe_properties": {
        "subscription_identifier": 1,
        "user_property": {
            "bar" : "foo"
        }
    }
}'
```

### サブスクライブのステータスの確認

登録したことで起動されるサブスクライバーのステータスを確認するには、次のコマンドを実行します。

```
$ curl "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/status"
[{"topic":"example\/#","upstream_uri":"127.0.0.1:9101","broker_uri":"127.0.0.1:1883","pid":124513}]
```

期待通りにサブスクライバーが起動できていると、登録時のパラメータが取得できます。

また、クエリパラメータでフィルターを指定することができます。

| パラメータ名  | 説明                                                             |
|---------------|------------------------------------------------------------------|
| broker_uris   | ブローカーのURI。同じパラメータを複数回指定可。                  |
| upstream_uris | 転送先のmqtt-httpプラグインのURI。同じパラメータを複数回指定可。 |
| topics        | サブスクライブしているトピック。同じパラメータを複数回指定可。   |

フィルター条件はURLエンコーディングする必要があります。

指定したフィルターに完全一致するステータスが返却されます。
あるパラメータに複数の条件を指定する場合は、以下のように同じパラメータを複数回指定します。

```
topics=topic1&topics=topic2
```

複数の条件を指定した場合、同じパラメータ間ではOR条件に、異なるパラメータ間ではAND条件になります。

例:

以下のコマンドを実行したケースを考えます。
ここで、 `%3A` はURLエンコードされた文字で、デコードすると`:`になります。

```
$ curl "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/status?topics=topic1&topics=topic2&upstream_uris=127.0.0.1%3A9100&upstream_uris=127.0.0.1%3A9101"
```

このフィルター条件は「（`topic`が`topic1`または`topic2`）かつ（`upstream_uri`が`127.0.0.1:9100`または`127.0.0.1:9100`）」となります。

### HTTPサーバーを起動する

HTTPサーバーを起動するには次のコマンドを実行します。

```
$ sudo dnf install -y python39
$ python3 /usr/local/apisix-plugin-mqtt-http-protocol-converter/tests/tools/python3/simple_web_server.py 8001 &
$ python3 /usr/local/apisix-plugin-mqtt-http-protocol-converter/tests/tools/python3/simple_web_server.py 8002 &
```

### MQTTメッセージをMQTTブローカーに送信する

MQTTメッセージをMQTTブローカーに送信するには、次のコマンドを実行します。

```
$ mosquitto_pub -h localhost -t example/test -m "test_message"
```

期待通りに送信できていると、次のようなメッセージが表示されます。

```
b'{"type":3,"properties":{},"user_properties":{},"dup":false,"qos":1,"topic":"example\\/test","type_name":"PUBLISH","payload":"test_message","packet_id":1,"retain":false}'        
127.0.0.1 - - [09/Jun/2022 10:46:18] "POST / HTTP/1.1" 200 -
```

ここまででMQTTメッセージをMQTTブローカーに送信し、APISIXを経由してMQTTからHTTPのリクエストに変換する動作を確認できました。

MQTTブローカーの登録を解除するには、次のコマンドを実行します。

```
$ curl -X POST -H "Content-Type: application/json" "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/unsubscribe" -d '
{
    "broker_uri" : "127.0.0.1:1883" ,
    "upstream_uri" : "127.0.0.1:9101",
    "topic" : "example/#"
}'
```

コマンドの実行に成功すると、`Unsubscribed`と表示されます。

なお、明示的に`unsubscribe`しなくても、APISIXのサービスを停止・再起動した場合にも、自動でMQTTブローカーの登録が解除されます。

## 制限

### QoS

現在本プラグインはMQTTバージョン5でのみQoS2をサポートしています。

### サブスクライブ

#### プロパティ

[MQTTブローカーに登録する](#MQTTブローカーに登録する)に記載の通り、サブスクライブ時に`CONNECT`及び`SUBSCRIBE`のプロパティを指定することが出来ます。

しかし、現在以下のプロパティはサポート対象外です。

* authentication_method
  * 現在本プラグインがAUTHパケットをサポートしていないため
* authentication_data
  * 現在本プラグインがAUTHパケットをサポートしていないため
* receive_maximum
  * 現在本プラグインでは常に1なため

## 補足情報

環境構築に関連した次の項目を説明します。

* 補足A. APISIXプラグインパッケージのビルド方法
* 補足B. APISIXプラグインのパッケージについて
* 補足C. APISIXプラグインの設定項目
* 補足D. MQTTSでのサブスクライバーの接続設定

### 補足A. APISIXプラグインパッケージのビルド方法

APISIXプラグインのパッケージをビルドするにはいくつか方法があります。

* a) MIRACLELINUX環境でパッケージをビルドする
* b) dockerでパッケージをビルドする

#### a) MIRACLELINUX環境でパッケージをビルドする

MIRACLELINUX環境でパッケージをビルドするには、すでに説明したAPISIXをインストールした上で次のコマンドを実行します。

```
$ sudo dnf install -y git gcc m4 rpmdevtools
$ rpmdev-setuptree
$ git clone https://gitlab.com/apisix-plugin-mqtt-http-protocol-converter/apisix-plugin-mqtt-http-protocol-converter.git
$ cd apisix-plugin-mqtt-http-protocol-converter
$ make rpm
```
APISIXプラグインのパッケージがビルドできたら、`~/rpmbuild/RPMS/x86_64/`以下にRPMが作成されます。

#### b) dockerでパッケージをビルドする

あらかじめ、`docker-compose`が実行できる環境であれば、次のコマンドを実行してパッケージをビルドすることができます。

```
$ git clone https://gitlab.com/apisix-plugin-mqtt-http-protocol-converter/apisix-plugin-mqtt-http-protocol-converter.git
$ cd apisix-plugin-mqtt-http-protocol-converter
$ docker-compose run --rm package /apisix-plugin-mqtt-http-protocol-converter/docker/package/build.sh
```

ビルドに成功すると、package/配下に`apisix-plugin-mqtt-http-protocol-converter-1.1.0-1.el8.x86_64.rpm`が作成されます。

### 補足B. APISIXプラグインのパッケージについて

APISIXプラグインのパッケージでは、APISIXパッケージを前提として、次のようなカスタマイズを実施しています。

* APISIXプラグインをAPISIXパッケージにならって`/usr/local/apisix-plugin-mqtt-http-protocol-converter`に配置
* APISIXプラグインに関連するモジュールを`/usr/local/apisix-plugin-mqtt-http-protocol-converter/deps`配下にインストール
* APISIXサービスと連携して動作できるように`/etc/systemd/system/apisix.service.d/override-apisix-3.conf`にて参照するプロファイルを変更
* APISIXの設定ファイルを`/usr/local/apisix/conf/config-mqtt-http-protocol-converter-apisix-3.yaml`に変更
* APISIXプラグインのサブスクライバーのログは`/usr/local/apisix-plugin-mqtt-http-protocol-converter/logs/relay-subscriber.log`に作成される (既定では10MiBごと、10世代保存)

また動作に必要な以下のモジュールをバンドルしています。

MQTT関連

* lua-cjson 2.1.0.10-1
* luamqtt 3.4.3-1
* cqueues 20200726.51-0

テスト関連

* busted 2.1.2-3
* dkjson 2.6-1
* lua-term 0.7-1
* lua_cliargs 3.0-2
* luafilesystem 1.8.0-1
* luassert 1.9.0-1
* luasystem 0.2.1-0
* mediator_lua 1.1.2-0
* penlight 1.13.1-1
* say 1.4.1-3

### 補足C. APISIXプラグインの設定項目

APISIXプラグインの設定項目は次のとおりです。
挙動を変更するには`/usr/local/apisix/conf/config-mqtt-http-protocol-converter-apisix-3.yaml`を編集します。
使用しているAPISIXがバージョン2の場合は`/usr/local/apisix/conf/config-mqtt-http-protocol-converter-apisix-2.yaml`を編集します。
一部のパラメータは使用しているAPISIXのバージョンによりYAMLの階層が変わります。
そのため、`config-mqtt-http-protocol-converter-apisix-3.yaml`と`config-mqtt-http-protocol-converter-apisix-2.yaml`では一部のパラメータのYAMLの階層が異なります。

* [config-mqtt-http-protocol-converter-apisix-2.yaml](../package/config-mqtt-http-protocol-converter-apisix-2.yaml)
* [config-mqtt-http-protocol-converter-apisix-3.yaml](../package/config-mqtt-http-protocol-converter-apisix-3.yaml)

以下では、YAMLの階層を`.`で表現しています。

| 項目                                                                                    | 値                                                                               | 説明                                                                                                                                             |
|-----------------------------------------------------------------------------------------|----------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|
| apisix.admin_key.name                                                                   | admin                                                                            | apisix-2のみ。変更不要                                                                                                                           |
| apisix.admin_key.key                                                                    | edd1c9f034335f136f87ad84b625c8f1                                                 | apisix-2のみ。プロダクション環境では変更推奨                                                                                                     |
| apisix.admin_key.role                                                                   | admin                                                                            | apisix-2のみ。変更不要                                                                                                                           |
| apisix.admin_listen                                                                     | 9180                                                                             | apisix-2のみ。管理系のエンドポイントのポートを指定する                                                                                           |
| apisix.stream_proxy.only                                                                | false                                                                            | 変更不要                                                                                                                                         |
| apisix.stream_proxy.tcp                                                                 | 9100,9101                                                                        | MQTTからHTTPへの変換を受け付けるポートを指定する                                                                                                 |
| apisix.extra_lua_path                                                                   | (省略)                                                                           | 変更不要。プラグインが読み込むLuaモジュールのパスを指定する                                                                                      |
| apisix.extra_lua_cpath                                                                  | (省略)                                                                           | 変更不要。プラグインが読み込むLuaモジュール(C実装)のパスを指定する                                                                               |
| nginx_config.http_configuration_snippet                                                 | (省略)                                                                           | 変更不要。プラグインがサブスクライバーの接続先を管理するために指定する                                                                           |
| nginx_config.http_configuration_snippet.lua_shared_dict mqtt_http_subscriber_status_dic | 10m                                                                              | プラグインがサブスクライバーのステータスを管理するための共有辞書のメモリサイズを指定する。10mは10MBの意味。不足する場合はサイズを増やす。        |
| nginx_config.stream_configuration_snippet.lua_shared_dict mqtt_http_packet_dic          | 10m                                                                              | 変更不要。プラグインがMQTTのメッセージを管理するための共有辞書のメモリサイズを指定する。10mは10MBの意味。不足する場合はサイズを増やす。          |
| nginx_config.stream_configuration_snippet.lua_ssl_trusted_certificate                   | なし                                                                             | httpsのサーバーにMQTTのメッセージを送信する場合に、信頼するCAの証明書のパスを指定する。                                                          |
| nginx_config.envs                                                                       | LUA_PATH,LUA_CPATH                                                               | 変更不要。プラグインがサブスクライバーを起動するのに必要な環境変数を有効にするために指定する                                                     |
| nginx_config.error_log_level                                                            | warn                                                                             | ログレベルを指定する。指定可能な値はnginxを参照すること。                                                                                        |
| plugins                                                                                 | public-api,mqtt-http-subscriber,node-status,log-rotate                           | 変更不要。有効にするプラグインを指定する。                                                                                                       |
| stream_plugins                                                                          | mqtt-http                                                                        | 変更不要。有効にするストリームプラグインを指定する。                                                                                             |
| etcd.host                                                                               | http://${{ETCD_HOST:=localhost}}:2379                                            | 変更不要。Etcdの接続先を指定する。`ETCD_HOST`環境変数を設定することで接続先を切り替えることができる。                                            |
| plugin_attr.log-rotate.interval                                                         | 86400                                                                            | ログローテーションの間隔を指定する。単位は秒で1日ごとに設定済み。                                                                                |
| plugin_attr.log-rotate.max_kept                                                         | 365                                                                              | ログローテーションで何世代保持するかを指定する。単位は世代数で365として設定済み。                                                                |
| plugin_attr.log-rotate.enable_compression                                               | true                                                                             | ログローテーションでログを圧縮の可否を指定する。圧縮ありで設定済み。                                                                             |
| plugin_attr.mqtt-http-subscriber.base_uri                                               | /apisix/plugin/mqtt-http-subscriber                                              | サブスクライバーのAPIにアクセスするときのエンドポイントとなるパスを指定する。                                                                    |
| plugin_attr.mqtt-http-subscriber.relay_subscriber_path                                  | /usr/local/apisix-plugin-mqtt-http-protocol-converter/tools/relay-subscriber.lua | 変更不要。サブスクライバーが起動する転送スクリプトの配置パスを指定する。APISIXプラグインをソースからインストールした場合にのみ変更が必要になる。 |
| plugin_attr.mqtt-http-subscriber.relay_subscriber_log_level                             | warn                                                                             | サブスクライバーのログレベルを指定する。サブスクライバーは別プロセスで動作するため専用の設定項目となっている。                                   |
| plugin_attr.mqtt-http-subscriber.relay_subscriber_log_max_file_size                     | 10485760                                                                         | サブスクライバーのログローテーションの基準となるファイルサイズを指定する。単位はバイト。                                                         |
| plugin_attr.mqtt-http-subscriber.relay_subscriber_log_max_backup_index                  | 10                                                                               | サブスクライバーのログローテーションで保持する世代数を指定する。                                                                                 |
| plugin_attr.mqtt-http-subscriber.cafile                                                 | なし                                                                             | MQTTS(SSL)接続する場合に使用するCA証明書へのパスを指定する。                                                                                     |
| plugin_attr.mqtt-http-subscriber.tls_version                                            | tlsv1_2                                                                          | MQTTS(SSL)接続する場合に使用するTLSのバージョンを指定する。                                                                                      |
| deployment.apisix.admin_key.name                                                        | admin                                                                            | apisix-3のみ。変更不要                                                                                                                           |
| deployment.apisix.admin_key.key                                                         | edd1c9f034335f136f87ad84b625c8f1                                                 | apisix-3のみ。プロダクション環境では変更推奨                                                                                                     |
| deployment.apisix.admin_key.role                                                        | admin                                                                            | apisix-3のみ。変更不要                                                                                                                           |
| deployment.etcd.host                                                                    | http://${{ETCD_HOST:=localhost}}:2379                                            | apisix-3のみ。変更不要。Etcdの接続先を指定する。`ETCD_HOST`環境変数を設定することで接続先を切り替えることができる。                              |
| deployment.role                                                                         | traditional                                                                      | apisix-3のみ。変更不要。[デプロイモード](https://apisix.apache.org/docs/apisix/deployment-modes/)を指定する。                                    |
| deployment.role_traditional.config_provider                                             | etcd                                                                             | apisix-3のみ。変更不要。設定のプロバイダーを指定する。                                                                                           |

### 補足D. MQTTSでのサブスクライバーの接続設定

VerneMQでMQTTSを有効にする手順は次のとおりです。
認証局証明書(ca.crt)ならびに、サーバー証明書(server.crt)、サーバー証明書の秘密鍵(server.key)はあらかじめ作成済みのものとします。

`/etc/vernemq/vernemq.conf`に次の設定を行います。

```
listener.ssl.default = 127.0.0.1:8883
listener.ssl.allowed_protocol_versions = 3,4,5
listener.ssl.cafile = /etc/vernemq/ca.crt
listener.ssl.certfile = /etc/vernemq/server.crt
listener.ssl.keyfile = /etc/vernemq/server.key
```

設定ができたら、VerneMQのサービスを再起動します。

```
$ sudo systemctl restart vernemq
```

サブスクライバーがMQTTS(TLS)接続する場合に使用するCA証明書へのパスを指定します。

`/usr/local/apisix/conf/config-mqtt-http-protocol-converter-apisix-3.yaml`の`plugin_attr.mqtt-http-subscriber.cafile`にCA証明書へのパスを指定します。

```
plugin_attr:
  mqtt-http-subscriber:
    cafile: "/etc/vernemq/ca.crt"
```

使用しているAPISIXがバージョン2の場合は`/usr/local/apisix/conf/config-mqtt-http-protocol-converter-apisix-2.yaml`を編集します。

設定が完了したら、APISIXのサービスを再起動します。

```
$ sudo systemctl restart apisix
```

サブスクライバーを登録するには次のコマンドを実行します。

```
$ curl -X POST -H "Content-Type: application/json" "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/subscribe" -d '
{
  "broker_uri" : "127.0.0.1:8883" ,
  "upstream_uri" : "127.0.0.1:9101",
  "user_name" : "henry",
  "password" : "hogehoge",
  "topic" : "example/#"
}'
```

サブスクライブに成功したあと、メッセージの送信テストを実施するには次のコマンドを実行します。

```
$ mosquitto_pub -d -h localhost -t example/test -u henry -P hogehoge -m "test_message" --cafile /etc/vernemq/ca.crt -p 8883
```

HTTPサーバーでメッセージが受け取れていれば、期待通りに動作しています。

### 補足E. HTTPSのWebサーバーへの接続設定

HTTPSのWebサーバーは作成済みで、必要な認証局証明書(ca.crt)ならびに、サーバー証明書(server.crt)、サーバー証明書の秘密鍵(server.key)は作成済みであるものとします。ここでは認証局証明書(ca.crt)のみ使用します。

mqtt-httpプラグインがHTTPS(TLS)接続する場合に、信頼するCAの証明書へのパスを指定します。

`/usr/local/apisix/conf/config-mqtt-http-protocol-converter-apisix-3.yaml`の`nginx_config.stream_configuration_snippet.lua_ssl_trusted_certificate `にCA証明書へのパスを指定します。

```
nginx_config:
  stream_configuration_snippet: |
    lua_ssl_trusted_certificate /etc/webserver/ca.crt;
```

使用しているAPISIXがバージョン2の場合は`/usr/local/apisix/conf/config-mqtt-http-protocol-converter-apisix-2.yaml`を編集します。

設定が完了したら、APISIXのサービスを再起動します。

```
$ sudo systemctl restart apisix
```

次に、`mqtt-http`プラグインを登録します。

すでに`mqtt-http`プラグインを登録済みの場合は、一度削除します。

```
$ curl http://127.0.0.1:9180/apisix/admin/stream_routes/1 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X DELETE
$ curl http://127.0.0.1:9180/apisix/admin/stream_routes/2 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X DELETE
```

`scheme`に`https`を指定して`mqtt-http`プラグインを登録します。

```
$ curl http://127.0.0.1:9180/apisix/admin/stream_routes/1 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "server_addr": "127.0.0.1",
    "server_port": 9100,
    "plugins": {
        "mqtt-http": {
            "protocol_version": 4,
            "path": "/path/to/test",
            "scheme": "https"
        }
    },
    "upstream": {
        "type": "roundrobin",
        "nodes": [{
            "host": "127.0.0.1",
            "port": 8001,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8002,
            "weight": 1
        }
        ]
    }
}'
$ curl http://127.0.0.1:9180/apisix/admin/stream_routes/2 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "server_addr": "127.0.0.1",
    "server_port": 9101,
    "plugins": {
        "mqtt-http": {
            "protocol_version": 5,
            "path": "/path/to/test",
            "scheme": "https"
        }
    },
    "upstream": {
        "type": "roundrobin",
        "nodes": [{
            "host": "127.0.0.1",
            "port": 8001,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8002,
            "weight": 1
        }
        ]
    }
}'
```

また、`ssl_verify: false`を指定することで、SSL証明書の検証をスキップすることも可能です。
自己署名証明書を使用する場合などは、こちらを指定してください。

```
        "mqtt-http": {
            "protocol_version": 5,
            "path": "/path/to/test",
            "scheme": "https",
            "ssl_verify": false
        }
```

以上でHTTPSのWebサーバーへの接続設定は完了です。

以下は設定が正しく行われているかの確認方法です。

サブスクライバーを登録します。一例は以下のとおりです。

```
$ curl -X POST -H "Content-Type: application/json" "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/subscribe" -d '
{
  "broker_uri" : "127.0.0.1:8883" ,
  "upstream_uri" : "127.0.0.1:9101",
  "user_name" : "henry",
  "password" : "hogehoge",
  "topic" : "example/#"
}'
```

サブスクライブに成功したあと、メッセージの送信テストを実施するには次のコマンドを実行します。

```
$ mosquitto_pub -d -h localhost -t example/test -u henry -P hogehoge -m "test_message" --cafile /etc/vernemq/ca.crt -p 8883
```

HTTPSのWebサーバーでメッセージが受け取れていれば、期待通りに動作しています。
