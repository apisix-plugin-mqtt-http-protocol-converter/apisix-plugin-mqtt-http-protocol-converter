local json = require("cjson.safe")
local protocol = require("mqtt.protocol")
local packet_type = protocol.packet_type

local luamqtt_to_mqtt_spec_key_mapper = {
    username = "user_name",
    type = "packet_type",
    id = "identifier",
    topic = "topic_name",
    rc = "reason_code",
    packet_id = "packet_identifier",
    sp = "session_present",
    subscription_identifiers = "subscription_identifier"
}

local nested_convert_target_table = {
    properties = true,
    will = true
}

local _M = {
    version = 0.1,
}

function convert_luamqtt_table_to_mqtt_spec_table(src_table)
    if not src_table then
        return {}
    end

    local dest_table = {}
    for src_key ,src_value in pairs(src_table) do
        local need_convert = nested_convert_target_table[src_key]
        local dst_value = need_convert and convert_luamqtt_table_to_mqtt_spec_table(src_value) or src_value
        local dst_key = luamqtt_to_mqtt_spec_key_mapper[src_key] or src_key
        dest_table[dst_key] = dst_value
    end
    return dest_table
end

function convert_mqtt_message_to_json(message)
    local destination = convert_luamqtt_table_to_mqtt_spec_table(message)
    destination.packet_type_name = packet_type[message.type]
    return json.encode(destination)
end
_M.convert_mqtt_message_to_json = convert_mqtt_message_to_json

return _M