# v1.2.5

## Release v1.2.5 - 2024/09/10

### Improvements

* Added support for filter specification to the status endpoint.
  * Please refer to [README.md](README.md#check-subscription-status-to-mqtt-broker) or [miraclelinux8.md](docs/miraclelinux8.md#サブスクライブのステータスの確認) for details
* Added `reconnect` parameter to the `subscribe` endpoint.
  * If `reconnect` is `true`, subscriber tries to reconnect to broker when communication is interrupted.
* Added `keep_alive` parameter to the `subscribe` endpoint.
  * Specify keep alive interval as seconds between subscriber and broker.
* Fixed an issue that MQTT user passwords were appeard to logs and process information.
  * The password was specified as the argument of relay-subscriber, so we can see the password using the `ps` command. Also, the
    commandline to execute relay-subscriber is logged to the APISIX's log with INFO level, therefore, the password is logged to the
    APISIX's log.
  * Now the password is specified with the standard input of relay-subscriber, so it does not appear in logs or process information.

### Bug fixes

* Fixed a bug that the `status` endpoint shows some already terminated subscribers.
  * The subscribers terminated with the `unsubscribe` endpoint were correctly determined as terminated.
  * In the previous versions, the subscribers terminated manually, e.g. using the kill command, were not determined as terminated.

## Release v1.2.4 - 2024/07/26

### Improvements

* Added support to post messages via TLS for HTTPS web server.

# v1.2.3

## Release v1.2.3 - 2024/05/30

### Improvements

* Added support for APISIX 3.8.0 or later.
  Because of changed package dependency in APISIX, use 1.2.3 for APISIX 3.8.0 or later.
  apisix-runtime is not required anymore.

# v1.2.2

## Release v1.2.2 - 2024/05/30

### Improvements

* Added support for APISIX 3.7.0.
  Because of changed package dependency in APISIX and OpenResty, use 1.2.2 for APISIX 3.7.0.
  apisix-base package is not required anymore. (If you want to use plugin with APISIX 3.6.0 or older, use 1.2.1)

# v1.2.1

## Release v1.2.1 - 2024/05/10

### Improvements

* Added support for specifying URI for nodes to which the mqtt-http plugin sends HTTP messages.
  * Please refer to [README.md](README.md#register-mqtt-http-plugin) or [miraclelinux8.md](docs/miraclelinux8.md#mqtt-httpプラグインを登録する) for details

### Bug fixes

* Fixed a bug that this plugin couldn't work with manually built APISIX 3.4.1+.
  * The required libraries are old, we updated them.
    * ext-plugin-proto 0.5.0 -> 0.6.0
    * lua-resty-etcd 1.7.0 -> 1.10.4

# v1.2.0

## Release v1.2.0 - 2023/08/29

### Improvements

* Added support for APISIX 3
  * The rpm package was separated to a main package and config packages.
    We should install the main package and an appropriate config package.
    Please refer to [miraclelinux8.md](docs/miraclelinux8.md#APISIXプラグインのインストール) for details.
* Added support for QoS2 under MQTT version 5
  * We cannot use QoS2 under MQTT versions earlier than 5.0
* Improve performance for QoS1
* Enabled to specify properties when sending CONNECT and SUBSCRIBE
  * Please refer to [README.md](README.md#subscribe-to-mqtt-broker) or [miraclelinux8.md](docs/miraclelinux8.md#mqttブローカーに登録する) for details
* Added support for the [Server Keep Alive](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901094) property of CONNACK.

### Miscellaneous

* Updated the required APISIX version to 2.15.3+
* Modified the default admin port to 9180 from 9080
  * 9180 is the default admin port of APISIX 3, we adjusted it accordingly.

# v1.1.0

## Release v1.1.0 - 2022/07/25

### Improvements

* MQTTS with CA file was supported.
* User name and password parameters for subscriber were supported.

### Bug fixes

* Fixed to set timeout for receiving connack from a broker.
  It fixes a bug that inconsistent "Subscribed" message may returns.

### Miscellaneous

* doc: Updated explanation about usage with `user_name` and `password` parameters.
* doc: Added explanation about `plugin_attr.cafile` and `plugin_attr.tls_version` configurations.
