#
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Request messages are created by tests/tools/lua/create_mqtt_test_packets.lua

use t::APISIX 'no_plan';

repeat_each(1);
no_long_string();
no_root_location();
no_shuffle();

add_block_preprocessor(sub {
    my ($block) = @_;

    my $extra_yaml_config = <<_EOC_;
stream_plugins:
    - mqtt-http
_EOC_

    $block->set_value("extra_yaml_config", $extra_yaml_config);

    my $http_config = $block->http_config // <<_EOC_;

    server {
        listen 8010;

        location / {
            content_by_lua_block {
                -- dummy http server
                local core = require("apisix.core")
                ngx.req.read_body()
                local body = ngx.req.get_body_data()
                core.log.info(body)
            }
        }

        location /testpath {
            content_by_lua_block {
                -- dummy http server
                local core = require("apisix.core")
                ngx.req.read_body()
                local body = ngx.req.get_body_data()
                core.log.info("/testpath")
                core.log.info(body)
            }
        }
    }

    server {
        listen 8011;

        location / {
            content_by_lua_block {
                ngx.exit(ngx.HTTP_NOT_FOUND)
            }
        }
    }
_EOC_

    $block->set_value("http_config", $http_config);

    if (!defined $block->extra_stream_config) {
        my $stream_config = $block->stream_config // <<_EOC_;
    lua_shared_dict mqtt_http_packet_dic 1m;
_EOC_

        $block->set_value("extra_stream_config", $stream_config);
    }
});
run_tests;

__DATA__

=== TEST 1: add plugin
--- config
    location /t {
        content_by_lua_block {
            local t = require("lib.test_admin").test
            local code, body = t('/apisix/admin/stream_routes/1',
                ngx.HTTP_PUT,
                [[{
                    "plugins": {
                        "mqtt-http": {
                            "protocol_version": 4
                        }
                    },
                    "upstream": {
                        "type": "roundrobin",
                        "nodes": [{
                            "host": "127.0.0.1",
                            "port": 8010,
                            "weight": 1
                        }]
                    },
                    "remote_addr": "127.0.0.1",
                    "server_port": 1985
                }]]
            )
            if code >= 300 then
                ngx.status = code
            end
            ngx.say(body)
        }
    }
--- request
GET /t
--- response_body
passed
--- no_error_log
[error]

=== TEST 2: invalid packet type
--- stream_request eval
"\xf0\x00"
--- error_log
unexpected packet type received

=== TEST 3: publish QoS0
--- stream_request eval
"\x30\x06\x00\x01\x74\x6d\x73\x67"
--- no_error_log
[error]

=== TEST 4: publish QoS1
--- stream_request eval
"\x32\x08\x00\x01\x74\x00\x01\x6d\x73\x67"
--- stream_response eval
"\x40\x02\x00\x01"
--- no_error_log
[error]

=== TEST 5: publish QoS2
--- stream_request eval
"\x34\x08\x00\x01\x74\x00\x01\x6d\x73\x67"
--- stream_response eval
"\x50\x02\x00\x01"
--- no_error_log
[error]

=== TEST 6: puback
--- stream_request eval
"\x40\x02\x00\x01"
--- no_error_log
[error]

=== TEST 7: pubrel
--- stream_request eval
"\x62\x02\x00\x01"
--- stream_response eval
"\x70\x02\x00\x01"
--- no_error_log
[error]

=== TEST 8: pubrec
--- stream_request eval
"\x50\x02\x00\x01"
--- no_error_log
[error]

=== TEST 9: pubcomp
--- stream_request eval
"\x70\x02\x00\x01"
--- no_error_log
[error]

=== TEST 10: bulk publish QoS1
--- stream_request eval
"\xff\x02\x32\x08\x00\x01\x74\x00\x01\x6d\x73\x67\x32\x08\x00\x01\x74\x00\x01\x6d\x73\x67"
--- stream_response eval
"\x40\x02\x00\x01\x40\x02\x00\x01"
--- no_error_log
[error]

=== TEST 11: add plugin with path
--- config
    location /t {
        content_by_lua_block {
            local t = require("lib.test_admin").test
            local code, body = t('/apisix/admin/stream_routes/1',
                ngx.HTTP_PUT,
                [[{
                    "plugins": {
                        "mqtt-http": {
                            "protocol_version": 4,
                            "path": "/testpath"
                        }
                    },
                    "upstream": {
                        "type": "roundrobin",
                        "nodes": [{
                            "host": "127.0.0.1",
                            "port": 8010,
                            "weight": 1
                        }]
                    },
                    "remote_addr": "127.0.0.1",
                    "server_port": 1985
                }]]
            )
            if code >= 300 then
                ngx.status = code
            end
            ngx.say(body)
        }
    }
--- request
GET /t
--- response_body
passed
--- no_error_log
[error]

=== TEST 12: check uri with publish QoS0
--- stream_request eval
"\x30\x06\x00\x01\x74\x6d\x73\x67"
--- error_log
/testpath

=== TEST 100: add 404 http-server
--- config
    location /t {
        content_by_lua_block {
            local t = require("lib.test_admin").test
            local code, body = t('/apisix/admin/stream_routes/1',
                ngx.HTTP_PUT,
                [[{
                    "plugins": {
                        "mqtt-http": {
                            "protocol_version": 4
                        }
                    },
                    "upstream": {
                        "type": "roundrobin",
                        "nodes": [{
                            "host": "127.0.0.1",
                            "port": 8011,
                            "weight": 1
                        }]
                    },
                    "remote_addr": "127.0.0.1",
                    "server_port": 1985
                }]]
            )
            if code >= 300 then
                ngx.status = code
            end
            ngx.say(body)
        }
    }
--- request
GET /t
--- response_body
passed
--- no_error_log
[error]

=== TEST 101: failed to send to http server QoS0
--- stream_request eval
"\x30\x06\x00\x01\x74\x6d\x73\x67"
--- no_error_log
[error]

=== TEST 102: failed to send to http server QoS1
--- stream_request eval
"\x32\x08\x00\x01\x74\x00\x01\x6d\x73\x67"
--- stream_response eval
"\x40\x02\x00\x01"
--- no_error_log
[error]

=== TEST 103: failed to send to http server QoS2
--- stream_request eval
"\x34\x08\x00\x01\x74\x00\x01\x6d\x73\x67"
--- stream_response eval
"\x50\x02\x00\x01"
--- no_error_log
[error]
