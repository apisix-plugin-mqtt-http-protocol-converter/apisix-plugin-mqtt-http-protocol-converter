from http.server import BaseHTTPRequestHandler, HTTPServer
import sys
import signal

def handler(signum, frame):
    sys.stdout.flush()
    exit(0)

signal.signal(signal.SIGTERM, handler)

if len(sys.argv) != 2 or sys.argv[1] == "-h" or sys.argv[1] == "--help":
  print(f'Usage: {sys.argv[0]} portnumber')
  print("Start simple httpserver on localhost")
  exit(1)

port = sys.argv[1]

class MyRequestHandler(BaseHTTPRequestHandler):
  def do_GET(self):
    print(self.path)
    self.my_send_response()

  def do_POST(self):
    content_length = int(self.headers.get('Content-Length')) 
    print(self.rfile.read(content_length))
    self.my_send_response()

  def my_send_response(self):
    self.protocol_version = "HTTP/1.1"
    self.send_response(200)
    self.send_header('Content-type', 'text/plain')
    # self.send_header("Connection", "keep-alive")
    # self.send_header("keep-alive", "timeout=1, max=3")
    self.end_headers()
    self.wfile.write("response".encode('utf-8'))


httpd = HTTPServer(('0.0.0.0', int(port)), MyRequestHandler)
httpd.serve_forever()
