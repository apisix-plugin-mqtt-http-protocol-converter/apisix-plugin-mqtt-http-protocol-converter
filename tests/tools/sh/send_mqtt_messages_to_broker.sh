#!/bin/bash

number=10000
if [ $# -gt 0 ]; then
    number=$1
fi

count=0
start_time=`date +%s`
echo "Number of messages:" $number
while [ $count -lt $number ]
do 
    mosquitto_pub -h localhost -p 1883 -t luamqtt/test -m "test_message_${count}" > /dev/null &
    count=$(($count + 1))
done

finish_time=`date +%s`
execution_time=$((finish_time - start_time))
echo "Execution time(sec):" $execution_time