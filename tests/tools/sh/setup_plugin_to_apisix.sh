#!/bin/bash

function prompt_help_and_exit () {
    echo "Usage: $0 APISIX_HOME_PATH"
    echo "Premise: * APISIX runs (port: 9080)"
    echo "         * etcd runs (port: 2379)"
    echo "         * mosquitto_pub is installed"
    echo "         * MQTT broker runs (port: 1883)"
    echo "         * HTTP server runs (port: 8001 and 8002)"
    echo "         * This plugin is installed"
    exit 1
}

if [ $# -ne 1 ] || [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
    prompt_help_and_exit
fi

apisix_home=$1

if [ ! -d $apisix_home ]; then
    prompt_help_and_exit
fi


pushd $apisix_home
    if [ $? -ne 0 ]; then
        exit 1
    fi
    apisix stop
    sleep 1
    apisix start
    if [ $? -ne 0 ]; then
        exit 1
    fi
popd

curl http://127.0.0.1:9180/apisix/admin/stream_routes/1 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X DELETE
curl http://127.0.0.1:9180/apisix/admin/stream_routes/2 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X DELETE
curl http://127.0.0.1:9180/apisix/admin/routes/1  -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X DELETE
curl http://127.0.0.1:9180/apisix/admin/routes/2  -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X DELETE
curl http://127.0.0.1:9180/apisix/admin/routes/3  -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X DELETE
curl http://127.0.0.1:9180/apisix/admin/routes/4  -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X DELETE

curl http://127.0.0.1:9180/apisix/admin/stream_routes/1 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "server_addr": "127.0.0.1",
    "server_port": 9100,
    "plugins": {
        "mqtt-http": {
            "protocol_version": 4
        }
    },
    "upstream": {
        "type": "roundrobin",
        "nodes": [{
            "host": "127.0.0.1",
            "port": 8001,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8002,
            "weight": 1
        }
        ]
    }
}'
curl http://127.0.0.1:9180/apisix/admin/stream_routes/2 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "server_addr": "127.0.0.1",
    "server_port": 9101,
    "plugins": {
        "mqtt-http": {
            "protocol_version": 5
        }
    },
    "upstream": {
        "type": "roundrobin",
        "nodes": [{
            "host": "127.0.0.1",
            "port": 8001,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8002,
            "weight": 1
        }]
    }
}'
curl -X PUT 'http://127.0.0.1:9180/apisix/admin/routes/2' -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/apisix/plugin/mqtt-http-subscriber/unsubscribe",
    "plugins": {
        "public-api": {}
    }
}'
curl -X PUT 'http://127.0.0.1:9180/apisix/admin/routes/3' -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/apisix/plugin/mqtt-http-subscriber/subscribe",
    "plugins": {
        "public-api": {}
    }
}'
curl -X PUT 'http://127.0.0.1:9180/apisix/admin/routes/4' -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/apisix/plugin/mqtt-http-subscriber/status",
    "plugins": {
        "public-api": {}
    }
}'

# test sample 

# curl -X POST -H "Content-Type: application/json" "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/subscribe" -d '
# {
#     "broker_uri" : "127.0.0.1:1883" ,
#     "upstream_uri" : "127.0.0.1:9101",
#     "topic" : "luamqtt/#"
# }'
# mosquitto_pub -d -h localhost -t luamqtt/test -m "test_message"
# mosquitto_pub -d -h localhost -t luamqtt/test -m "test_message2"
# mosquitto_pub -d -h localhost -t luamqtt/test -m "test_message3"
# mosquitto_pub -d -h localhost -t luamqtt/test -m "test_message4"
# mosquitto_pub -d -h localhost -t luamqtt/test -m "test_message5"
# curl -X GET "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/status"
# curl -X POST -H "Content-Type: application/json" "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/unsubscribe" -d '
# {
#     "broker_uri" : "127.0.0.1:1883" ,
#     "upstream_uri" : "127.0.0.1:9101",
#     "topic" : "luamqtt/#"
# }'
# curl -X GET "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/status"
