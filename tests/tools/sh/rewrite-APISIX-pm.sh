#!/bin/bash

apisix_home=$1
apisix_extra_plugin_home=${APISIX_EXTRA_PLUGIN_HOME:-$apisix_home}
apisix_package_home=${APISIX_PACKAGE_HOME:-$apisix_home}

lua_path="${apisix_extra_plugin_home}/?.lua;"
lua_path="${lua_path}${apisix_extra_plugin_home}/deps/share/lua/5.1/?/init.lua;"
lua_path="${lua_path}${apisix_extra_plugin_home}/deps/share/lua/5.1/?.lua;"
lua_path="${lua_path}${apisix_package_home}/?.lua;"
lua_path="${lua_path}${apisix_package_home}/deps/share/lua/5.1/?/init.lua;"
lua_path="${lua_path}${apisix_package_home}/deps/share/lua/5.1/?.lua;"
lua_path="${lua_path}\$apisix_home/?.lua;"
lua_path="${lua_path}\$apisix_home/?/init.lua;"
lua_path="${lua_path}\$apisix_home/deps/share/lua/5.1/?/init.lua;"
lua_path="${lua_path}\$apisix_home/deps/share/lua/5.1/?.lua;"
lua_path="${lua_path}\$apisix_home/apisix/?.lua;"
lua_path="${lua_path}\$apisix_home/t/?.lua;"
lua_path="${lua_path}\$apisix_home/t/xrpc/?.lua;"
lua_path="${lua_path}\$apisix_home/t/xrpc/?/init.lua;"
lua_path="${lua_path};"

lua_cpath="${apisix_extra_plugin_home}/?.lua;"
lua_cpath="${lua_cpath}${apisix_extra_plugin_home}/deps/lib/lua/5.1/?.so;"
lua_cpath="${lua_cpath}${apisix_package_home}/?.so;"
lua_cpath="${lua_cpath}${apisix_package_home}/deps/lib/lua/5.1/?.so;"
lua_cpath="${lua_cpath}\$apisix_home/plugins/?.so;"
lua_cpath="${lua_cpath}\$apisix_home/?.so;"
lua_cpath="${lua_cpath}\$apisix_home/deps/lib/lua/5.1/?.so;"
lua_cpath="${lua_cpath}\$apisix_home/deps/lib64/lua/5.1/?.so;"
lua_cpath="${lua_cpath};"

sed -i'' -e 's|lua_package_path .*$|lua_package_path "'${lua_path}'";|g' ${apisix_home}/t/APISIX.pm
sed -i'' -e 's|lua_package_cpath .*$|lua_package_cpath "'${lua_cpath}'";|g' ${apisix_home}/t/APISIX.pm
sed -i'' -e 's|env PATH;|env LUA_PATH;\nenv LUA_CPATH;\nenv PATH;|g' ${apisix_home}/t/APISIX.pm
