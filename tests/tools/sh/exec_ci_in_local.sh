#!/bin/bash

function prompt_help_and_exit () {
    echo "Usage: $0 APISIX_HOME_PATH"
    echo "Premise: * etcd runs (port: 2379)"
    exit 1
}

if [ $# -ne 1 ] || [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
    prompt_help_and_exit
fi

plugin_home=${PWD}/../../..
apisix_home=$1

cp -rf ${plugin_home}/src/plugins/* $apisix_home
cp -rf ${plugin_home}/src/* $apisix_home

export APISIX_EXTRA_PLUGIN_HOME=${plugin_home}
# Customize APISIX.pm to enable APISIX_PACKAGE_HOME, APISIX_EXTRA_PLUGIN_HOME
pushd $apisix_home
  git checkout ${apisix_home}/t/APISIX.pm
popd
./rewrite-APISIX-pm.sh $apisix_home

cp -f ${plugin_home}/tests/apisix/t/stream-plugin/mqtt-http*.t ${apisix_home}/t/stream-plugin
cp -f ${plugin_home}/tests/apisix/t/plugin/mqtt-http-subscriber.t ${apisix_home}/t/plugin

# Disable ldap-auth plugin which cause unexpected loading plugin error.
sed -i'' -e 's/- ldap-auth/#- ldap-auth/' ${apisix_home}/conf/config-default.yaml

# Use MQTT specific configuration
cp -f ${plugin_home}/src/conf/config.yaml ${apisix_home}/conf/config.yaml

export LUA_PATH="${LUA_PATH};${apisix_home}/?.lua;${apisix_home}/?/init.lua"
export LUA_CPATH="${LUA_CPATH};${apisix_home}/?.so"

pushd $apisix_home
    LUA_PATH=$LUA_PATH TEST_NGINX_BINARY=/usr/bin/openresty prove -I. -I./t t/stream-plugin/mqtt-http*.t t/plugin/mqtt-http-subscriber.t
popd
