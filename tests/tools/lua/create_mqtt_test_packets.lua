local protocol = require("mqtt.protocol")
local packet_type = protocol.packet_type
local protocol5 = require("mqtt.protocol5")
local protocol4 = require("mqtt.protocol4")

local function print_hex_string(message)
    local result = ""
    for i = 1, #message do
        char = string.sub(message, i, i)
        result = result.."\\x"..string.format("%02x", string.byte(char))
    end
    print(result)
end

local function print_message_table_as_hex_string(table, make_packet_func)
    for k, v in pairs(table) do
        print(k)
        local packet = make_packet_func(v)
        print_hex_string(tostring(packet))
    end
end

local v5_message_table = {
    publish_qos0 = {
        type = packet_type.PUBLISH,
        topic = "t",
        payload = "msg",
        qos = 0,
        packet_id = 1,
        retain = false,
        dup = false,
        properties = {
            response_topic = "t",
            subscription_identifiers = { 111 }
        },
        user_properties = { 
            a = "b",
        },
    },
    publish_qos1 = {
        type = packet_type.PUBLISH,
        topic = "t",
        payload = "msg",
        qos = 1,
        packet_id = 1,
        retain = false,
        dup = false,
        properties = {
            response_topic = "t",
            subscription_identifiers = { 111 }
        },
        user_properties = { 
            a = "b",
        },
    },
    publish_qos1_puback = {
        type = packet_type.PUBACK,
        rc = 0,
        packet_id = 1,
    },
    publish_qos1_puback_when_err = {
        type = packet_type.PUBACK,
        rc = 1,
        packet_id = 1,
    },
    publish_qos2 = {
        type = packet_type.PUBLISH,
        topic = "t",
        payload = "msg",
        qos = 2,
        packet_id = 1,
        retain = false,
        dup = false,
        properties = {
            response_topic = "t",
            subscription_identifiers = { 111 }
        },
        user_properties = { 
            a = "b",
        },
    },
    puback = {
        type = packet_type.PUBACK,
        rc = 2,
        packet_id = 1,
        properties = {
            reason_string = "rs",
        },
        user_properties =  {
            a = "b",
        },
    },
    pubrec = {
        type = packet_type.PUBREC,
        rc = 0,
        packet_id = 1,
        properties = {
            reason_string = "rs",
        },
        user_properties =  {
            a = "b",
        },
    },
    pubrel = {
        type = packet_type.PUBREL,
        rc = 0,
        packet_id = 1,
        properties = {
            reason_string = "rs",
        },
        user_properties =  {
            a = "b",
        },
    },
    pubcomp = {
        type = packet_type.PUBCOMP,
        rc = 0,
        packet_id = 1,
        properties = {
            reason_string = "rs",
        },
        user_properties =  {
            a = "b",
        },
    },
    disconnect = {
        type = packet_type.DISCONNECT,
        rc = 1,
        properties = {
            session_expiry_interval = 1,
            server_reference = "sr",
            reason_string = "rs",
        },
        user_properties =  {
            a = "b",
        },
    },
    auth = {
        type = packet_type.AUTH,
        rc = 1,
        properties = {
            authentication_method = "am",
            authentication_data = "ad",
            reason_string = "rs",
        },
        user_properties =  {
            a = "b",
        },
    },
}

local v4_message_table = {
    publish_qos0 = {
        type = packet_type.PUBLISH,
        topic = "t",
        payload = "msg",
        qos = 0,
        packet_id = 1,
        retain = false,
        dup = false,
    },
    publish_qos1 = {
        type = packet_type.PUBLISH,
        topic = "t",
        payload = "msg",
        qos = 1,
        packet_id = 1,
        retain = false,
        dup = false,
    },
    publish_qos1_puback = {
        type = packet_type.PUBACK,
        rc = 0,
        packet_id = 1,
    },
    publish_qos1_puback_when_err = {
        type = packet_type.PUBACK,
        rc = 1,
        packet_id = 1,
    },
    publish_qos2 = {
        type = packet_type.PUBLISH,
        topic = "t",
        payload = "msg",
        qos = 2,
        packet_id = 1,
        retain = false,
        dup = false,
    },
    puback = {
        type = packet_type.PUBACK,
        rc = 2,
        packet_id = 1,
    },
    pubrec = {
        type = packet_type.PUBREC,
        rc = 0,
        packet_id = 1,
    },
    pubrel = {
        type = packet_type.PUBREL,
        rc = 0,
        packet_id = 1,
    },
    pubcomp = {
        type = packet_type.PUBCOMP,
        rc = 0,
        packet_id = 1,
    },
    disconnect = {
        type = packet_type.DISCONNECT,
        rc = 1,
    },
}

print("v5")
print_message_table_as_hex_string(v5_message_table, protocol5.make_packet)
print("v4")
print_message_table_as_hex_string(v4_message_table, protocol4.make_packet)