local protocol5 = require("mqtt.protocol5")
local socket = require("socket")
local protocol = require("mqtt.protocol")
local packet_type = protocol.packet_type
local argparse = require("argparse")

local parser = argparse("script", "MQTT relay subscriber.")
parser:option("-n --number", "Number of messages", 30000)
parser:option("-t --target", "Target URI(MQTT v5). e.g. localhost:9101", "localhost:9101")

local args = parser:parse()

local message = {
    type = packet_type.PUBLISH,
    topic = "test/topic",
    payload = "test message",
    qos = 1,
    packet_id = 1,
    retain = false,
    dup = false,
}
local packet = protocol5.make_packet(message)
local data = tostring(packet)
local count = 0
local num = args.number


local target_host, target_port_str = string.match(args.target, "^([^%s]+):(%d+)$")
local target_port = tonumber(target_port_str)

print("Number of messages:", num)
print("Target URI:", args.target)

start_time = os.time()
while count < num do
    local client = socket.connect(target_host, target_port)
    client:send(data)
    client:close()
    count = count + 1
end
print("Execution time(sec):", os.time() - start_time)
