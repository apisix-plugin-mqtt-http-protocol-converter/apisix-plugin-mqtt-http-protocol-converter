-- LuaRocks configuration

rocks_trees = {
   { name = "user", root = home .. "/.luarocks" };
   { name = "system", root = "/usr/local" };
   { name = "apisix", root = "/usr/local/apisix/deps" };
   { name = "apisix-plugin-mqtt-http-protocol-converter", root = "/usr/local/apisix-plugin-mqtt-http-protocol-converter/deps" };
}
lua_interpreter = "luajit";
variables = {
   LUA_DIR = "/usr/local/openresty/luajit";
   LUA_BINDIR = "/usr/local/openresty/luajit/bin";
}
