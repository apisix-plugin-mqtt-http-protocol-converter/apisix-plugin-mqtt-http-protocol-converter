%global debug_package %{nil}
%global __requires_exclude ^libssl|libcrypto.*$
%define _apisixdir %{_exec_prefix}/local/%{name}

%if 0%{?rhel} == 8
# https://pagure.io/packaging-committee/issue/738
%define __brp_mangle_shebangs /usr/bin/true
%endif

Name: apisix-plugin-mqtt-http-protocol-converter
Version: 1.2.6
Release: 1%{?dist}
Summary: APISIX plugin which convert MQTT to HTTP
Group: System Environment/Daemons
License: Apache-2.0
URL: https://gitlab.com/apisix-plugin-mqtt-http-protocol-converter/apisix-plugin-mqtt-http-protocol-converter
Source: %{name}-%{version}.tar.gz
BuildArch: x86_64

Requires: apisix >= 3.8.0
Requires: (%{name}-config-apisix-3 or %{name}-config-apisix-2)
Conflicts: apisix-base <= 1.21.4.2.2
Conflicts: openresty-pcre <= 8.45
Conflicts: openresty-zlib <= 1.2.13
%{systemd_requires}

%description
APISIX plugin which convert MQTT to HTTP.  APISIX plugin behaves like
a MQTT subscriber, then convert received MQTT to specific HTTP
requests.

%package config-apisix-2
Summary: Config files for %{name}-%{version} working with APISIX 2
Provides: %{name}-config
Conflicts: %{name}-config-apisix-3

%description config-apisix-2
Config files for %{name}-%{version} working with APISIX 2

%package config-apisix-3
Summary: Config files for %{name}-%{version} working with APISIX 3
Provides: %{name}-config
Conflicts: %{name}-config-apisix-2

%description config-apisix-3
Config files for %{name}-%{version} working with APISIX 3

%prep
%setup

%build

%install
rm -rf %{buildroot}
PREFIX=%{_prefix} EXEC_PREFIX=%{_exec_prefix} ROOT_PATH=%{buildroot} SYSCONF_DIR=%{_sysconfdir} APISIX_DIR=%{_apisixdir} make install

%pre
%post
%systemd_post apisix.service

%preun
%systemd_preun apisix.service

%postun
%systemd_postun_with_restart apisix.service

%files
%defattr(-,root,root,-)
%doc %{_prefix}/local/share/%{name}/README.md
%attr(755,nobody,root) %{_apisixdir}/logs
%dir %{_apisixdir}/logs
%config %{_sysconfdir}/sysconfig/%{name}
%{_apisixdir}/apisix/stream/plugins/mqtt-http.lua
%{_apisixdir}/apisix/plugins/mqtt-http-subscriber.lua
%{_apisixdir}/deps/*
%{_apisixdir}/tools/*
%{_apisixdir}/tests/*
%{_apisixdir}/version

%files config-apisix-2
%defattr(-,root,root,-)
%config %{_prefix}/local/apisix/conf/config-mqtt-http-protocol-converter-apisix-2.yaml
%config %{_prefix}/local/apisix/conf/debug-mqtt-http-protocol-converter-apisix-2.yaml
%config %{_sysconfdir}/systemd/system/apisix.service.d/override-apisix-2.conf

%files config-apisix-3
%defattr(-,root,root,-)
%config %{_prefix}/local/apisix/conf/config-mqtt-http-protocol-converter-apisix-3.yaml
%config %{_prefix}/local/apisix/conf/debug-mqtt-http-protocol-converter-apisix-3.yaml
%config %{_sysconfdir}/systemd/system/apisix.service.d/override-apisix-3.conf

%changelog
* Thu Sep 17 2024 Takashi Hashida <hashida@clear-code.com> - 1.2.6-1
- New upstream release
- Added support for getting detailed status via "/apisix/plugin/mqtt-http-subscriber/status?detail=true"

* Thu Sep 10 2024 Takashi Hashida <hashida@clear-code.com> - 1.2.5-1
- New upstream release
- Added support for filter specification to the status endpoint.
- Added "reconnect" parameter to the "subscribe" endpoint.
- Added "keep_alive" parameter to the "subscribe" endpoint.
- Fixed an issue that MQTT user passwords were appeard to logs and process information.
- Fixed a bug that the "status" endpoint shows some already terminated subscribers.

* Fri Jul 26 2024 Takashi Hashida <hashida@clear-code.com> - 1.2.4-1
- New upstream release
- Added support to post messages via TLS for HTTPS web server.

* Thu May 30 2024 Kentaro Hayashi <hayashi@clear-code.com> - 1.2.3-1
- New upstream release
- Support apisix 3.8.0 or later.

* Thu May 30 2024 Kentaro Hayashi <hayashi@clear-code.com> - 1.2.2-1
- New upstream release
- Just support apisix 3.7.0 only.

* Tue Aug 29 2023 Takashi Hashida <hashida@clear-code.com> - 1.2.0-1
- New upstream release
- Submoduled each configs for APISIX 2 and APISIX 3

* Mon Jul 25 2022 Kentaro Hayashi <hayashi@clear-code.com> - 1.1.0-1
- New upstream release

* Thu Jul 7 2022 Kentaro Hayashi <hayashi@clear-code.com> - 1.0.1-1
- Fixed a bug which has QoS1 performance regression

* Thu Jul 7 2022 Kentaro Hayashi <hayashi@clear-code.com> - 1.0.0-1
- initial release
