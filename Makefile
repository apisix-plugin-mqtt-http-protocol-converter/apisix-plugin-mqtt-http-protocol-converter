PACKAGE=apisix-plugin-mqtt-http-protocol-converter
VERSION=1.2.6
ARCHIVE=$(PACKAGE)-$(VERSION)

PREFIX ?= /usr
EXEC_PREFIX ?= /usr
ROOT_PATH ?= /
SYSCONF_DIR ?= /etc
APISIX_DIR ?= /usr/local/apisix
APISIX_REPOSITORY ?= /tmp

all: 
	rm -rf dest
	mkdir -p dest
	cp -R src/ dest/
	cp -R tests/apisix/t dest

rpm:
	git archive HEAD --prefix $(ARCHIVE)/ -o $(ARCHIVE).tar && \
	  mkdir -p $(ARCHIVE) && \
	  git log -n 1 --date=iso --format='%h %cd' > $(ARCHIVE)/version && \
	  tar rvf $(ARCHIVE).tar $(ARCHIVE)/version && \
	  gzip -f $(ARCHIVE).tar && \
	  rm -fr $(ARCHIVE)
	cp $(ARCHIVE).tar.gz ~/rpmbuild/SOURCES/
	# QA_RPATHS $((0x0001|0x0002))
	LANG=C QA_RPATHS=3 rpmbuild -ba package/apisix-plugin-mqtt-http-protocol-converter.spec

list-luarocks:
	rpm -qpl package/apisix-plugin-mqtt-http-protocol-converter-*.rpm | \grep rocks-5.1 | cut -d'/' -f9-10 | sort | uniq | grep "/"

install-to-apisix-repository:
	install src/conf/config.yaml $(APISIX_REPOSITORY)/conf/
	for d in stream/plugins plugins; do \
	  install -d $(APISIX_REPOSITORY)/apisix/$$d; \
	  install src/plugins/apisix/$$d/*.lua $(APISIX_REPOSITORY)/apisix/$$d/; \
	done
	install -d $(APISIX_REPOSITORY)/tools
	install src/plugins/tools/*.lua $(APISIX_REPOSITORY)/tools/
	for d in mqtt-to-json tools/lua; do \
	  install -d $(APISIX_REPOSITORY)/tests/$$d; \
	  install tests/$$d/*.lua $(APISIX_REPOSITORY)/tests/$$d; \
	done
	install -d $(APISIX_REPOSITORY)/tests/tools/sh
	install tests/tools/sh/*.sh $(APISIX_REPOSITORY)/tests/tools/sh/
	install -d $(APISIX_REPOSITORY)/tests/tools/python3
	install tests/tools/python3/*.py $(APISIX_REPOSITORY)/tests/tools/python3/
	install tests/apisix/t/*.pm $(APISIX_REPOSITORY)/t/
	install tests/apisix/t/stream-plugin/*.t $(APISIX_REPOSITORY)/t/stream-plugin/
	install tests/apisix/t/plugin/*.t $(APISIX_REPOSITORY)/t/plugin/

install:
	mkdir -p $(ROOT_PATH)$(APISIX_DIR)/apisix/stream/plugins
	mkdir -p $(ROOT_PATH)$(APISIX_DIR)/apisix/plugins
	mkdir -p $(ROOT_PATH)$(PREFIX)/local/share/$(PACKAGE)
	mkdir -p $(ROOT_PATH)$(PREFIX)/local/apisix/conf/apisix-2
	mkdir -p $(ROOT_PATH)$(PREFIX)/local/apisix/conf/apisix-3
	mkdir -p $(ROOT_PATH)$(SYSCONF_DIR)/systemd/system/apisix.service.d
	mkdir -p $(ROOT_PATH)$(SYSCONF_DIR)/sysconfig
	cp README.md $(ROOT_PATH)$(PREFIX)/local/share/$(PACKAGE)
	cp package/config-mqtt-http-protocol-converter-apisix-2.yaml $(ROOT_PATH)$(PREFIX)/local/apisix/conf/
	cp package/debug-mqtt-http-protocol-converter-apisix-2.yaml $(ROOT_PATH)$(PREFIX)/local/apisix/conf/
	cp package/config-mqtt-http-protocol-converter-apisix-3.yaml $(ROOT_PATH)$(PREFIX)/local/apisix/conf/
	cp package/debug-mqtt-http-protocol-converter-apisix-3.yaml $(ROOT_PATH)$(PREFIX)/local/apisix/conf/
	cp -a src/plugins/apisix/stream/plugins/mqtt-http.lua $(ROOT_PATH)$(APISIX_DIR)/apisix/stream/plugins/
	cp -a src/plugins/apisix/plugins/mqtt-http-subscriber.lua $(ROOT_PATH)$(APISIX_DIR)/apisix/plugins/
	cp README.md $(ROOT_PATH)$(PREFIX)/local/share/$(PACKAGE)
	cp package/override-apisix-2.conf $(ROOT_PATH)$(SYSCONF_DIR)/systemd/system/apisix.service.d/override-apisix-2.conf
	cp package/override-apisix-3.conf $(ROOT_PATH)$(SYSCONF_DIR)/systemd/system/apisix.service.d/override-apisix-3.conf
	cp -r src/plugins/tools $(ROOT_PATH)$(APISIX_DIR)/
	cp -r tests $(ROOT_PATH)$(APISIX_DIR)/
	cp package/$(PACKAGE) $(ROOT_PATH)$(SYSCONF_DIR)/sysconfig/
	cp version $(ROOT_PATH)$(APISIX_DIR)/
	# Avoid to conflict with /usr/local/apisix/deps/lib/luarocks/rocks-5.1/manifest
	# Install luarocks under separate %{_apisixdir}
	# Then, add apisix and apisix-plugin-mqtt-http-protocol-converter environments to
	# /usr/local/etc/luarocks/config-5.1.lua.
	#
	# rocks_trees = {
	#   { name = "user", root = home .. "/.luarocks" };
	#   { name = "system", root = "/usr/local" };
	#   { name = "apisix", root = "/usr/local/apisix/deps" };
	#   { name = "apisix-plugin-mqtt-http-protocol-converter", root = "/usr/local/apisix-plugin-mqtt-http-protocol-converter/deps" };
	#}
	$(EXEC_PREFIX)/local/bin/luarocks install luamqtt --deps-mode all --tree $(ROOT_PATH)/$(APISIX_DIR)/deps
	$(EXEC_PREFIX)/local/bin/luarocks install lua-cjson --tree $(ROOT_PATH)/$(APISIX_DIR)/deps
	LD_LIBRARY_PATH=/usr/local/openresty/openssl3/lib PKG_CONFIG_PATH=/usr/local/openresty/openssl3/lib/pkgconfig $(EXEC_PREFIX)/local/bin/luarocks install cqueues --tree $(ROOT_PATH)/$(APISIX_DIR)/deps CRYPTO_DIR=/usr/local/openresty/openssl3 OPENSSL_DIR=/usr/local/openresty/openssl3
	# For testing
	$(EXEC_PREFIX)/local/bin/luarocks install busted --tree $(ROOT_PATH)/$(APISIX_DIR)/deps
	# Strip embedded $(ROOT_PATH) in lua scrits
	sed -i'' -e s,$(ROOT_PATH),,g $(ROOT_PATH)/$(APISIX_DIR)/deps/bin/json2lua
	sed -i'' -e s,$(ROOT_PATH),,g $(ROOT_PATH)/$(APISIX_DIR)/deps/bin/lua2json
	sed -i'' -e s,$(ROOT_PATH),,g $(ROOT_PATH)/$(APISIX_DIR)/deps/bin/busted
	mkdir -p $(ROOT_PATH)/$(APISIX_DIR)/logs
