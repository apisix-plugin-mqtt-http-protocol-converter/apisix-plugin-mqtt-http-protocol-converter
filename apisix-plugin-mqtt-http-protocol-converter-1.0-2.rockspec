package = "apisix-plugin-mqtt-http-protocol-converter"
version = "1.0-2"
rockspec_format = "3.0"
source = {
    url = "..."
}
description = {
   summary = "APISIX plugin which convert MQTT to HTTP",
   detailed = [[
       APISIX plugin which convert MQTT to HTTP.  APISIX plugin behaves like
       a MQTT subscriber, then convert received MQTT to specific HTTP
       requests.
   ]],
   homepage = "https://gitlab.com/apisix-plugin-mqtt-http-protocol-converter/apisix-plugin-mqtt-http-protocol-converter",
   license = "Apache-2.0",
   issues_url = "https://gitlab.com/apisix-plugin-mqtt-http-protocol-converter/apisix-plugin-mqtt-http-protocol-converter/-/issues",
   maintainer = "Clear Code inc.",
   labels = {
      "mqtt",
      "apisix"
   }
}
supported_platforms = {
   "linux",
   "macosx"
}
dependencies = {
   "lua >= 5.1, < 5.4",
   "cqueues ~> 20200726.51",
   "api7-dkjson ~> 0.1.1",
   "api7-lua-resty-http ~> 0.2.0",
   "api7-lua-tinyyaml ~> 0.4.2",
   "argparse ~> 0.7.1",
   "base64 ~> 1.5",
   "binaryheap ~> 0.4",
   "cqueues ~> 20200726.51",
   "dkjson ~> 2.6",
   "ext-plugin-proto ~> 0.6.0",
   "graphql ~> 0.0.2",
   "inspect ~> 3.1.3",
   "jsonschema ~> 0.9.8",
   "lpeg ~> 1.0.2",
   "lrandom ~> 20180729",
   "lrexlib-pcre ~> 2.9.1",
   "lua-protobuf ~> 0.3.4",
   "lua-resty-balancer ~> 0.04",
   "lua-resty-cookie ~> 0.1.0",
   "lua-resty-ctxdump ~> 0.1",
   "lua-resty-dns-client ~> 6.0.2",
   "lua-resty-etcd ~> 1.10.4",
   "lua-resty-expr ~> 1.3.0",
   "lua-resty-hmac-ffi ~> 0.06",
   "lua-resty-http ~> 0.17.0.beta.1",
   "lua-resty-ipmatcher ~> 0.6.1",
   "lua-resty-jit-uuid ~> 0.0.7",
   "lua-resty-jwt ~> 0.2.3",
   "lua-resty-kafka ~> 0.20",
   "lua-resty-logger-socket ~> 2.0.1",
   "lua-resty-mediador ~> 0.1.2",
   "lua-resty-ngxvar ~> 0.5.2",
   "lua-resty-openidc ~> 1.7.5",
   "lua-resty-openssl ~> 0.8.8",
   "lua-resty-radixtree ~> 2.8.2",
   "lua-resty-rocketmq ~> 0.4.1",
   "lua-resty-session ~> 3.10",
   "lua-resty-template ~> 2.0",
   "lua-resty-timer ~> 1.1.0",
   "lua-resty-worker-events ~> 2.0.1",
   "lua-tinyyaml ~> 1.0",
   "lua-typeof ~> 0.1",
   "luafilesystem ~> 1.8.0",
   "lualogging ~> 1.6.0",
   "luamqtt ~> 3.4.2",
   "luaposix ~> 35.1",
   "luasec ~> 1.1.0",
   "luasocket ~> 3.0.0",
   "luaxxhash ~> 1.0.0",
   "net-url ~> 1.1",
   "penlight ~> 1.12.0"
}
build = {
   type = "builtin",
   modules = {
      ["apisix.plugins.mqtt-http-subscriber"] = "src/plugins/apisix/plugins/mqtt-http-subscriber.lua",
      ["apisix.stream.plugins.mqtt-http"] = "src/plugins/apisix/stream/plugins/mqtt-http.lua",
      ["tools.mqtt-to-json"] = "src/plugins/tools/mqtt-to-json.lua",
      ["tools.relay-subscriber"] = "src/plugins/tools/relay-subscriber.lua"
   },
   copy_directories = {
      "docs",
      "tests"
   }
}
