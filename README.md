# apisix-plugin-mqtt-http-protocol-converter

apisix-plugin-mqtt-http-protocol-converter is a plugin for Apache APISIX which converts MQTT protocol to transfer them as an specific HTTP request.

## Prerequisites

* Apache APISIX 3.8.0 or later
* Etcd 3.4.0 or later
* LuaRocks (optional)
* cqueues/20200726.51-0
* luamqtt 3.4.2-3
* lua-cjson 2.1.0.6-1

Additionally, the following LuaRocks modules are required for testing.

* busted 2.0.0-1
* dkjson 2.6-1
* lua-term 0.7-1
* lua_cliargs 3.0-2
* luafilesystem 1.8.0-1
* luassert 1.8.0-0
* luasystem 0.2.1-0
* mediator_lua 1.1.2-0
* penlight 1.12.0
* say 1.3-1

## Installation

For MIRACLELINUX 8.4, see [Install instruction for MIRACLELINUX 8](docs/miraclelinux8.md) (Written in Japanese)

For other environment, See [Install instructions](docs/install.md)

## Usage

If you already installed all prerequisites components, following steps explain how to use APISIX plugin.

In this example, all prerequisites components are assumed working on the same host.

To make it easy for you to get started with this plugin, here's a list of recommended steps.

* Register `mqtt-http` plugin
* Enable public API
* Subscribe to MQTT broker
* Start HTTP server which works as backends
* Send MQTT message to MQTT broker

In the following example, MQTT messages are sent to MQTT broker, then protocol is converted into HTTP via
APISIX plugin and forwarded into HTTP servers (roundrobin for backend 2 HTTP servers)

### Register `mqtt-http` plugin

Execute the following command to register `mqtt-http` plugin for MQTT protocol version 4.

```
curl http://127.0.0.1:9180/apisix/admin/stream_routes/1 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "server_addr": "127.0.0.1",
    "server_port": 9100,
    "plugins": {
        "mqtt-http": {
            "protocol_version": 4,
            "path": "/path/to/test"
        }
    },
    "upstream": {
        "type": "roundrobin",
        "nodes": [{
            "host": "127.0.0.1",
            "port": 8001,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8002,
            "weight": 1
        }
        ]
    }
}'
```

It means that APISIX accepts MQTT message in port 9100, then forward converted HTTP messages into backend HTTP servers (`http://127.0.0.1:8001/path/to/test`, `http://127.0.0.1:8002/path/to/test` are used)

Execute the following command to register `mqtt-http` plugin for MQTT protocol version 5.

```
curl http://127.0.0.1:9180/apisix/admin/stream_routes/2 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "server_addr": "127.0.0.1",
    "server_port": 9101,
    "plugins": {
        "mqtt-http": {
            "protocol_version": 5,
            "path": "/path/to/test"
        }
    },
    "upstream": {
        "type": "roundrobin",
        "nodes": [{
            "host": "127.0.0.1",
            "port": 8001,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8002,
            "weight": 1
        }
        ]
    }
}'
```

It means that APISIX accepts MQTT message in port 9101, then forward converted HTTP messages into backend HTTP servers (`http://127.0.0.1:8001/path/to/test`, `http://127.0.0.1:8002/path/to/test` are used)


### Enable public API

Execute the following command to register public API.

```
$ curl 'http://127.0.0.1:9180/apisix/admin/routes/1' -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/apisix/plugin/mqtt-http-subscriber/unsubscribe",
    "plugins": {
        "public-api": {}
    }
}'
$ curl 'http://127.0.0.1:9180/apisix/admin/routes/2' -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/apisix/plugin/mqtt-http-subscriber/subscribe",
    "plugins": {
        "public-api": {}
    }
}'
$ curl 'http://127.0.0.1:9180/apisix/admin/routes/3' -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/apisix/plugin/mqtt-http-subscriber/status",
    "plugins": {
        "public-api": {}
    }
}'
```

It means that 3 API are registered - subscribe, unsubscribe, and status.

### Subscribe to MQTT broker

To notify MQTT broker how to connect to APISIX server, execute the following command.

```
$ curl -X POST -H "Content-Type: application/json" "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/subscribe" -d '
{
    "broker_uri" : "127.0.0.1:1883" ,
    "upstream_uri" : "127.0.0.1:9101",
    "topic" : "example/#"
}'
```

If the above command succeeds, `Subscribed` message is shown on console.

You can use the following parameters.

| Parameter            | Type        | Default                                                          | Explanation                                                                           |
|----------------------|-------------|------------------------------------------------------------------|---------------------------------------------------------------------------------------|
| broker_uri           | String      | "localhost:1883"                                                 | Broker URI.                                                                           |
| upstream_uri         | String      | "localhost:9100"                                                 | Transfer target mqtt-http plugin URI.                                                 |
| topic                | String      | "/#"                                                             | Topic to subscribe.                                                                   |
| mqtt_version         | String      | "5"                                                              | MQTT version. "4" for MQTT v3.1.1, and "5" for MQTT v5.0                              |
| user_name            | String      | stPwSVV73Eqw5LSv0iMXbc4EguS7JyuZR9lxU5uLxI5tiNM8ToTVqNpu85pFtJv9 | User name to connect broker.                                                          |
| password             | String      | null                                                             | Password to connect broker.                                                           |
| keep_alive           | Integer     | 60                                                               | Keep alive interval as seconds between broker and executed subscriber.                |
| connect_properties   | JSON string | null                                                             | Properties of `CONNECT` packet. *1                                                    |
| subscribe_properties | JSON string | null                                                             | Properties of `SUBSCRIBE` packet. *1                                                  |
| reconnect            | Boolean     | false                                                            | Whether to automatically reconnect when communication with broker is interrupted. |

*1

The properties should be specified with JSON key name in [json_key_and_mqtt_spec.md](docs/json_key_and_mqtt_spec.md).

Please refer to the following links for available properties.

[CONNECT Properties](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901046)
[SUBSCRIBE Properties](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901164)

The following properties are currently not supported.

* authentication_method
  * Because this plugin don't support AUTH packet yet.
* authentication_data
  * Because this plugin don't support AUTH packet yet.
* receive_maximum
  * Because this plugin's receive maximum is always 1 for now.


E.g.

To specify `user_name` and `password`, execute the following command.

```
$ curl -X POST -H "Content-Type: application/json" "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/subscribe" -d '
{
"broker_uri" : "127.0.0.1:1883" ,
"upstream_uri" : "127.0.0.1:9101",
"user_name" : "user",
"password" : "password",
"topic" : "example/#"
}'
```

To specify `connect_properties` and `subscribe_properties`, execute the following command.

```
$ curl -X POST -H "Content-Type: application/json" "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/subscribe" -d '
{
    "broker_uri" : "127.0.0.1:1883" ,
    "upstream_uri" : "127.0.0.1:9101",
    "topic" : "example/#",
    "connect_properties": {
        "session_expiry_interval": 10,
        "maximum_packet_size": 1024
    },
    "subscribe_properties": {
        "subscription_identifier": 1,
        "user_property": {
            "bar" : "foo"
        }
    }
}'
```

### Check subscription status to MQTT broker

To check the status about the subscriber that has been started by registering it, execute the following command:

```
$ curl "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/status"
[{"topic":"example\/#","upstream_uri":"127.0.0.1:9101","broker_uri":"127.0.0.1:1883","pid":124513}]
```

If the subscriber is started as expected, you can obtain the parameters at the registration.

You can also specify filters in query parameters.

| Parameter     | Explanation                                                                          |
|---------------|--------------------------------------------------------------------------------------|
| broker_uris   | Broker URIs. The parameter can be specified multiple times.                          |
| upstream_uris | Transfer target mqtt-http plugin URIs. The parameter can be specified multiple times |
| topics        | Subscribed topics. The parameter can be specified multiple times.                    |

Filter conditions must be URL encoded.

Subscription status that exactly matches the specified filter will be returned.
If you want to specify multiple conditions for a parameter, specify the same parameter multiple times as below.

```
topics=topic1&topics=topic2
```

If multiple conditions are specified, OR condition will be applied between the same parameters, and an AND condition will be applied between different parameters.

E.g.

Consider the case where the following command is executed.
`%3A` is a URL-encoded character that decodes to `:`.

```
$ curl "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/status?topics=topic1&topics=topic2&upstream_uris=127.0.0.1%3A9100&upstream_uris=127.0.0.1%3A9101"
```

This filter condition is "(`topic` is `topic1` or `topic2`) and (`upstream_uri` is `127.0.0.1:9100` or `127.0.0.1:9100`)".

### Start HTTP server which works as backends

Execute the following command to launch backend HTTP servers.

```
$ python3 tests/tools/python3/simple_web_server.py 8001 &
$ python3 tests/tools/python3/simple_web_server.py 8002 &
```

### Send MQTT message to MQTT broker

Execute the following command to send message to MQTT broker.

```
$ mosquitto_pub -h localhost -t example/test -m "test_message"
```

Then, in backend HTTP server side, the following converted message is shown.

```
b'{"type":3,"properties":{},"user_properties":{},"dup":false,"qos":1,"topic":"example\\/test","type_name":"PUBLISH","payload":"test_message","packet_id":1,"retain":false}'        
127.0.0.1 - - [09/Jun/2022 10:46:18] "POST / HTTP/1.1" 200 -
```

## Limitation

### QoS

This plugin support QoS2 only under MQTT version 5.

### Subscribe

#### Properties

As described in [Subscribe to MQTT broker](#subscribe-to-mqtt-broker), you can specify properties of `CONNECT` and `SUBSCRIBE` when subscribing.

However, the following properties are currently not supported.

* authentication_method
  * Because this plugin don't support AUTH packet yet.
* authentication_data
  * Because this plugin don't support AUTH packet yet.
* receive_maximum
  * Because this plugin's receive maximum is always 1 for now.

## Appendix

### MQTT conversion rules

To know how MQTT data are converted into JSON key name, see [JSON key and MQTT spec](docs/json_key_and_mqtt_spec.md)

### MIRACLELINUX 8.4

To install APISIX plugin for MIRACLELINUX 8.4, see [Install instruction for MIRACLELINUX 8](docs/miraclelinux8.md) (Written in Japanese)


## License

[Apache 2.0 License](https://gitlab.com/apisix-plugin-mqtt-http-protocol-converter/apisix-plugin-mqtt-http-protocol-converter/LICENSE)

